#
# Nixos lets you override all the variables that define the system. This can be
# a bit overwhelming. In this file, we summarise the values that your are
# likely to want to change.
#
# search.nixos.org is convenient to quickly look for available configuration
# options. If you need to override a value that is already set in another
# configuration file, we recommend that you do it here or in bridge-specific
# files with the mkForce function. eg:
#   services.tor.enable = lib.mkForce false;
# This makes it easier to upgrade the base configuration later without loosing
# your local settings.
#

{ config, pkgs, lib, ... }:

{

  # Use a SSH key to log-in as root on the bridges.
  # This key should be well protected.
  # Copy here the content of the public key file.
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3N...RF6j+"
  ];

  # The ssh server listens by default behind an onion service. In addition, it
  # can also listen on a virtual interface connected to Yggdrasil, a P2P IPv6
  # overlay network that can be used to access the nodes without going through
  # Tor. This typically makes the shell more responsive, but might establish a
  # link between your bridges, if you manage several of them in different
  # places.
  torbridge.useYggdrasil = false;
  # Both the onion service and Yggdrasil establish only outgoing connections.
  # Thus they work without needing open ports on the home router. This allows
  # reliable remote access to the nodes under most circumstances.

  # If you want to set up an obfs4 bridge, provide a contact string. It is only
  # used by the Tor Project team in case something weird happens whith you
  # bridge. This string is not private and read by humans, so you can obfuscate
  # a bit.
  services.tor.settings.ContactInfo = "address[AT]domain.tld";

  # Choose whether this bridge should collect metrics and send them to a
  # monitoring server. Check this project for a NixOS configuration for the
  # monitoring server.
  torbridge.monitoring.enable = false;
  # If a monitoring server is used, specify its onion address.
  torbridge.monitoring.monitoringServerOnion = "fljasa...duid.onion";
}
