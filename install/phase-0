#!/usr/bin/env bash

set -euo pipefail

# THIS SCRIPT IS MEANT TO BE CALLED BY `install-torbridge` ONLY.
# It should be run on a machine to install a minimal nixos system with an ssh
# server allowing remote root access for further configuration. It
# interactively prompts for the storage device on which nixos should be
# installed.

# List available block devices and prompt to select an installation device.
lsblk

echo "On which device should I install NixOS ?"
read -r grub_device

# Format the selected disk.
# We boot the system with legacy BIOS, so we create a single MBR partition that
# fills the whole disk.
echo "Creating a MBR partition on ${grub_device}"
sfdisk "${grub_device}" <<-EOF
	label: dos
	- - - *
	EOF

nixos_rootfs="${grub_device}1"

# Create a filesystem on the partition. Override any existing filesystem.
echo "Creating a filesystem on ${nixos_rootfs}"
mkfs.ext4 -FF "${nixos_rootfs}"

tmproot="/mnt"
echo "Mouting the partition at ${tmproot}"
mkdir -p "${tmproot}"
mount "${nixos_rootfs}" "${tmproot}"

# Store the installation device in /root of the installed system. This allows
# the next installation step to recover it and inject it in the final
# configuration.nix.
mkdir -p "${tmproot}/root"
echo "${grub_device}" > "${tmproot}/root/grub-device"

echo "Setting-up NixOS channel"
nix-channel --add https://nixos.org/channels/nixos-23.11-small nixos
nix-channel --update

echo "Generating NixOS configuration"
nixos-generate-config --root "${tmproot}"

cat > "${tmproot}/etc/nixos/configuration.nix" <<-EOF 
	{ config, lib, pkgs, ... }: 

	{

	  imports = [
	    ./hardware-configuration.nix
	  ];

	  boot.loader.grub.device = "${grub_device}";

	  networking.hostName = "bootstrap-sshd";

	  environment.systemPackages = with pkgs; [
		vim
		mkp224o			# Used to provision onion address.
		openssl			# Used to provision authentication key pair for 
		basez			# the monitoring onion service.
		gnused
	  ];

	  services.openssh = {
		enable = true;
		settings.PermitRootLogin = "yes";
	  };

	  users.users.root = {
		password = "";
		openssh.authorizedKeys.keys = [
		  "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILfbhEsYX/5zg083k8vztajjAYN+jSll+4swR56ChbkD"
		];
	  };

	  system.stateVersion = lib.version;
	}
	EOF

echo "Installing NixOS"
nixos-install --no-root-password

echo "Installation complete"
echo "Rebooting"
reboot
