#!/usr/bin/env bash

#
# This script installs a tor bridge on a machine booted on the bootstrap image.
#

set -euo pipefail

print_help () {
	cat <<-EOF
	Install a new torbridge on a machine booted on the bootstrap image.

	Usage: $(basename "${BASH_SOURCE[0]}") BRIDGENAME ADDRESS

	Arguments:
	  * BRIDGENAME: Nickname for the bridge.
	  * ADDRESS: IP address of the machine on the LAN, booted on the bootstrap
	             image.
	EOF
}

if [[ -z "$1" || -z "$2" ]]
then
	print_help
	exit 1
fi

bridgename="$1"
machine="$2"

script_dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
generic_config_dir="$(readlink -f "${script_dir}/../config")"
bridge_configs_dir="$(readlink -f "${script_dir}/../bridge_configs")"
bridge_data_dir="$(readlink -f "${bridge_configs_dir}/data")"

bridgeconfig="$(readlink -f "${bridge_configs_dir}/bridge-${bridgename}.nix")"

# SSH options to allow connections using the passwordless bootstrap key.
ssh_key="${script_dir}/../bootstrap/id_ed25519_bootstrap"
ssh_opts=(-i "${ssh_key}"
		  -o 'StrictHostKeyChecking no'
		  -o 'UserKnownHostsFile /dev/null')

#--- Check whether a configuration file already exists for the bridge.

if [[ -f ${bridgeconfig} ]]
then
	echo "A configuration for bridge ${bridgename} already exists in"	\
		 "${bridgeconfig}. Using it."
fi

#--- Execute phase-0 script on the target machine to install a minimal NixOS.

scp "${ssh_opts[@]}" "${script_dir}/phase-0" "root@${machine}":

# Disable exit on error, to let the script continue after the ssh connection is
# closed by the machine reboot.
set +e
ssh -t "${ssh_opts[@]}" "root@${machine}" ./phase-0
set -e

#--- Generate a configuration for the new bridge.

echo "Wait for the machine to reboot. Then press [enter]."
read -r 

# Collect the device on which grub is installed.
grub_device="$(ssh "${ssh_opts[@]}" "root@${machine}" cat /root/grub-device)"

"${script_dir}/generate-bridge-config" "${bridgename}"	\
	--grub-device "${grub_device}"

echo "Take some time to check that you are happy with the configuration"	\
	 "generated in ${bridgeconfig}, edit if necessary. Then, press enter"   \
	 "to continue the installation."
read -r 

#--- Provision an onion address and a monitoring public key on the bridge.

scp "${ssh_opts[@]}" "${script_dir}/phase-1" "root@${machine}:"

ssh -t "${ssh_opts[@]}" "root@${machine}" ./phase-1

mkdir -p "${bridge_data_dir}/${bridgename}"

# Collect the provisioned onion address and monitoring public key and store
# them in the data store.
ssh "${ssh_opts[@]}" "root@${machine}"		\
	cat /root/secrets/onionkeys/hostname	\
	> "${bridge_data_dir}/${bridgename}/onion-address"
ssh "${ssh_opts[@]}" "root@${machine}"		\
	cat /root/secrets/metricskey.pub		\
	> "${bridge_data_dir}/${bridgename}/monitoring-pubkey"

#--- Install the new bridge.

# Copy the generic configuration files.
scp "${ssh_opts[@]}"												\
	"${generic_config_dir}"/* "${bridge_configs_dir}/common.nix"	\
	"root@${machine}:/etc/nixos/"

# Copy the bridge-specific configuration.
scp "${ssh_opts[@]}"		\
	"${bridgeconfig}" "root@${machine}:/etc/nixos/node.nix"

# Build the bridge configuration and reboot into the installed bridge.
ssh "${ssh_opts[@]}" "root@${machine}" nixos-rebuild boot
set +e
ssh "${ssh_opts[@]}" "root@${machine}" reboot
set -e

echo 'Installation complete ! One more bridge in the wild \( ‘з’)/'
echo 'If you configured the bridge to send metrics to the monitoring server, make sure to authorise the public key.'
