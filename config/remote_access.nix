#
# This module sets up a ssh server for remote access to the nodes.
#
# The ssh server listens by default behind an onion service. In addition, it
# can also listen on a virtual interface connected to Yggdrasil, a P2P IPv6
# overlay network that can be used to access the nodes without going through
# Tor. This typically makes the shell more responsive, but might establish a
# link between your bridges, if you manage several of them in different places.
#
# Both the onion service and Yggdrasil establish only outgoing connections.
# Thus they work without needing open ports on the home router. This allows
# reliable remote access to the nodes under most circumstances.
#

{ config, lib, ... }:

with lib;
{

  options.torbridge.useYggdrasil = mkOption {
    description = ''If set, the node is connected to the Yggdrasil overlay
                    network which provides an alternative remote access route
                    that does not go through Tor.'';
    type = types.bool;
    default = false;
  };

  config = {

    # Enable the OpenSSH daemon.
    services.openssh = {
      enable = true;
      # Only listen on localhost.
      # FIXME: If Yggdrasil is enabled, then listen on all interfaces since we
      # don't know the Yggdrasil address...
      listenAddresses = mkIf (! config.torbridge.useYggdrasil) [{
        addr = "127.0.0.1";
        # According to the documentation, it shouldn't be necessary to specify
        # a port here since it is already (implicitely) set in
        # services.openssh.ports. However, the check-sshd-config in
        # system.checks of the sshd module seem to require a port to be set.
        # FIXME
        port = 22;
      }];
      # Use keys for authentication.
      settings = {
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
    };

    # Enable Fail2ban to protect from bruteforce attacks against the ssh server
    # and decrease logs verbosity. The sshd jail is enabled by default.
    services.fail2ban.enable = true;

    # Create a container to run a Tor daemon isolated from the bridge daemon.
    # The bridge daemon should only be used for bridge traffic. The daemon in
    # the container only acts as a client of the Tor network. Here, we use it
    # to set up an onion service that exposes the ssh server.
    containers."bridgecontrol" =
      let
        sshports = config.services.openssh.ports;
        hostname = config.networking.hostName;
      in
      {
        autoStart = true;
        privateNetwork = false;

        config = { config, pkgs, ... }: {

          services.tor = {
            enable = true;
            client.enable = true;
            # By default, the tor daemon waits for 1m30s to allow graceful
            # termination of circuits. We reduce it for faster shutdown since
            # the daemon is only acting as a client and does not manage
            # people's circuits.
            settings.ShutdownWaitLength = 1;

            # Create an onion service to expose the ssh server.
            relay.onionServices."bridgecontrol" = {
              # Use a provisionned private key to generate a deterministic
              # onion address for the service. The private key need to be
              # already present on the machine, only readable by root.
              secretKey = "/root/secrets/onionkeys/hs_ed25519_secret_key";
              # Map each port on which the ssh server listens on the same port
              # on the onion service.
              map = map
                (sshport:
                  {
                    port = sshport;
                    target = {
                      addr = "127.0.0.1";
                      port = sshport;
                    };
                  }
                )
                sshports;
            };
          };

          system.stateVersion = "23.11";
        };
      };

    # Inject the onion service keys into the container.
    system.activationScripts.inject_onionkeys = ''
      mkdir -p /var/lib/nixos-containers/bridgecontrol/root/secrets
      cp -r /root/secrets/onionkeys   \
            /var/lib/nixos-containers/bridgecontrol/root/secrets
    '';

    # Connect to the Yggdrasil network. Yggdrasil is P2P IPv6 overlay network.
    # The Yggdrasil service creates a virtual network interface connected to
    # the network. Only ougoing connections are made to preconfigured peers, so
    # no open ports are need if the machine is behing a NAT.
    services.yggdrasil = {
      enable = config.torbridge.useYggdrasil;
      settings = {
        # Select peers to connect to the network. A list of public peers can be
        # found at https://github.com/yggdrasil-network/public-peers. It is a
        # good idea to select multiple peers. This provides redundancy and
        # makes it harder for an observer to see that several machines receive
        # traffic from a common source.
        Peers = [
          "tls://[2001:41d0:304:200::ace3]:23108"
          "tls://51.255.223.60:54232"
          "tcp://51.15.204.214:55555"
        ];
        NodeInfoPrivacy = true;
        # Do not publicly announce the Yggdrasil node.
        MulticastInterfaces = [{
          Regex = ".*";
          Beacon = false;
          Listen = false;
          Port = 0;
        }];
      };
      # The address of the Yggdrasil interface is in fact an authentication
      # key.  By default, the key (and hence the address) is generated once and
      # then kept. If you set this option to 'false', a new key is generated at
      # every boot. If you choose this option, make sure that you have a way to
      # retrieve the new IP in case of reboot. Having one Yggdrasil node you
      # control as peer of the machine is one way.
      persistentKeys = true;
    };
  };
}
