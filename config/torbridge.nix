#
# This module is the entry point to the configuration that is specific to
# building a Tor bridge. Here again, we try to organise the configuration in
# meaningful modules to ease hacking.
#

{ config, pkgs, lib, ... }:

with builtins;
with lib;
{

  options.torbridge = {
    pluggableTransport = mkOption {
      description = ''Pluggable transport used by the bridge. Only one can be 
                      used on a given bridge because hosting two bridge types on 
                      one IP risks blocking the two if any one is blocked.'';
      type = types.enum [ "obfs4" "snowflake" ];
    };
  };

  imports = [
    ./remote_access.nix
    ./upnpc.nix
    ./monitoring.nix
    ./torbridge_obfs4.nix
    ./torbridge_snowflake.nix
  ];

  config = {
    # Use our custom prometheus-tor-exporter !
    # For some reason, nix ends-up in infinite recursion if we put this into
    # the monitoring module, where it would better fit (*ﾟｰﾟ)ゞ
    nixpkgs.overlays = [ (import ./prometheus-tor-exporter_override.nix) ];
  };
}
