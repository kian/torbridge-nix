self: super: {
  prometheus-tor-exporter = super.prometheus-tor-exporter.overrideAttrs
    (finalAttrs: previousAttrs: {
      src = super.fetchFromGitHub {
        owner = "oaksoaj";
        repo = "prometheus-tor_exporter";
        rev = "299d62103eb6cb0b7220f1db6f4355c5cf20844f";
        sha256 = "TCzpqkyjWU0Ix526Ya2dNL/UrTifVcTBanhrnb11+us=";
      };
    });
}

