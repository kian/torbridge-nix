# This file is the entry point of the full system configuration. We tried to
# break up the configuration in meaningful modules.

{ config, pkgs, ... }:

{
  imports = [
    # Results of the hardware scan.
    ./hardware-configuration.nix
    # Basic system configuration.
    ./base_system.nix
    # Torbridge configuration.
    ./torbridge.nix
    # Deployment-specific configuration.
    ./common.nix
    # Node-specific configuration.
    ./node.nix
  ];
}
