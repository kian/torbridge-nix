#
# This module opens ports on the home router for the bridge traffic.
# Ports are opened using the UPnP protocol. Routers implementations of UPnP are
# not 100% reliable. We sometimes have to open ports manually using the web
# interface of the home router.
#

{ config, pkgs, lib, ... }:

with lib;
{

  options.torbridge.openPortsUPnP = mkOption {
    description = ''Periodically send UPnP requests to the router to open 
                    the listed ports.'';
    type = types.listOf types.port;
    default = [ ];
  };

  config = mkIf (config.torbridge.openPortsUPnP != [ ]) {

    networking.firewall = {
      # Accept udp connections emitted from a port 1900. This is necessary to
      # receive home router's reply to UPnP requests.
      extraCommands = ''
        iptables -A INPUT -j ACCEPT -p udp --sport 1900
      '';
      extraStopCommands = ''
        # Revert rules on stop.
        iptables -D INPUT -j ACCEPT -p udp --sport 1900
      '';
    };

    # Setup one cron jobs per port to open with miniupnpc.
    services.cron = {
      enable = true;
      systemCronJobs = map
        (port:
          "*/5 * * * * root ( date; ${pkgs.miniupnpc}/bin/upnpc -e bridge -r ${toString port} TCP ) >> /var/log/upnpc.log 2>&1")
        config.torbridge.openPortsUPnP;
    };

    # Rotate upnpc logs.
    services.logrotate = {
      enable = true;
      settings."/var/log/upnpc.log" = {
        frequency = "weekly";
        missingok = true;
        rotate = 20;
        notifempty = true;
      };
    };
  };
}
