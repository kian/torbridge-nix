#
# Nixos lets you override all the variables that define the system. This can be
# a bit overwhelming. In this file, we summarise the values that your are
# likely to want to change.
#
# search.nixos.org is convenient to quickly look for available configuration
# options. If you need to override a value that is already set in another
# configuration file, we recommend that you do it here or in common.nix
# with the mkForce function. eg:
#   services.tor.enable = lib.mkForce false;
# This makes it easier to upgrade the base configuration later without loosing
# your local settings.
#

{ config, pkgs, lib, ... }:

{
  # Host name of the node.
  networking.hostName = ___BRIDGE_NAME___;

  # The device on which NixOS is installed. Install grub in the MBR.
  boot.loader.grub.device = ___GRUB_DEVICE___;

  # Select the type of bridge to deploy. For now, only "obfs4" is supported.
  torbridge.pluggableTransport = "obfs4";
  # Choose a random OnionRouter port for the bridge, used to communicate with
  # the rest of the tor network.
  torbridge.obfs4.orport = ___ORPORT___;
  # Choose a random Obfs4 port to receive bridge client connections.
  torbridge.obfs4.obfs4port = ___OBFS4PORT___;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}
