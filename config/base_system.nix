#
# This module contains the configuration of the bare system, aside from its
# Tor bridge role.
# It implements some hardening and fail-safe mecanisms to maximise the chances
# that we can recover machines without intervention of those who host them.
#

{ config, pkgs, ... }:

let
  testPing = pkgs.writeScriptBin "test-ping" ''
    # A small script that pings quad9 and reboots the machine if the connection
    # is down. This can help if the network card crashes for instance.
    if ! ${pkgs.inetutils}/bin/ping -c 10 9.9.9.9 | ${pkgs.gnugrep}/bin/grep " bytes from "; then
      ${pkgs.systemd}/bin/reboot
    else
      echo "Internet connectivity check successfull at: $(date)" > /tmp/pingresult
    fi
  '';
in
{

  # Bootloader configuration. We use legacy BIOS to ensure compatibility with
  # all hardware.
  boot.loader.grub.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Choose a keyboard layout for the console.
  console.keyMap = "de";

  # Mount /proc with hidepid. With this setting, unpriviledged users can only
  # see their processes.
  # Note however that systemd still exposes the pid, commandline arguments, uid
  # and gid of system services to any user at the API level... See for instance
  # https://access.redhat.com/solutions/6704531.
  fileSystems."/proc" = {
    device = "proc";
    fsType = "proc";
    options = [ "nosuid" "nodev" "noexec" "hidepid=2" ];
  };

  # We use a hardened kernel from the Nixos repositories. It is not very clear
  # whether this is useful with the explicit (and more restrictive) boot and
  # sysctl parameters we set after that, but it certainly cannot hurt.
  # In order to improve stability and limit the need of reboots after upgrades,
  # we freeze the kernel version to 6.1 which is a LTS that will receive
  # security updates until December 2026.
  # If you want to run a bridge on very recent hardware, you might need to use
  # a more recent kernel with updated drivers.
  boot.kernelPackages = pkgs.linuxPackages_6_1_hardened;

  # A bunch of extra hardening parameters passed to the kernel at boot time.
  boot.kernelParams = [
    "page_poison=1"
    "slab_nomerge"
    "slub_debug=FZP"
    "mce=0"
    "pti=on"
    "mds=full,nosmt"
    "init_on_free=1"
    "init_on_alloc=1"
    "lockdown=confidentiality"
    "intel_iommu=on"
    "amd_iommu=on"
    "modules.sig_enforce=1"
    "vsyscall=none"
    "oops=panic"
  ];

  # Yet another bunch of hardening parameters passed to sysctl.
  boot.kernel.sysctl = {
    "kernel.dmesg_restrict" = 1;
    "kernel.kptr_restrict" = 2;
    "kernel.unprivileged_bpf_disabled" = 1;
    "net.core.bpf_jit_harden" = 2;
    "kernel.yama.ptrace_scope" = 2;
    "kernel.kexec_load_disabled" = 1;
    "net.ipv4.tcp_syncookies" = 1;
    "net.ipv4.tcp_rfc1337" = 1;
    "net.ipv4.conf.default.rp_filter" = 1;
    "net.ipv4.conf.all.rp_filter" = 1;
    "net.ipv4.conf.all.accept_redirects" = 0;
    "net.ipv4.conf.default.accept_redirects" = 0;
    "net.ipv6.conf.all.accept_redirects" = 0;
    "net.ipv6.conf.default.accept_redirects" = 0;
    "net.ipv4.conf.all.send_redirects" = 0;
    "net.ipv4.conf.default.send_redirects" = 0;
    "net.ipv4.icmp_echo_ignore_all" = 1;
    "vm.mmap_rnd_bits" = 32;
    "vm.mmap_rnd_compat_bits" = 16;
    "kernel.sysrq" = 0;
    "kernel.deny_new_usb" = 1;
    "kernel.panic" = 10;
    "net.ipv4.conf.all.arp_filter" = 1;
    "net.ipv4.conf.all.arp_ignore" = 2;
    "net.ipv4.conf.all.arp_announce" = 2;
    "net.ipv4.conf.all.drop_gratuitous_arp" = 1;
    "net.ipv4.tcp_syn_retries" = 3;
    "net.ipv4.tcp_synack_retries" = 3;
    "kernel.panic_on_oops" = 1;
    "kernel.perf_event_max_sample_rate" = 1;
    "kernel.perf_cpu_time_max_percent" = 1;
    "kernel.perf_event_paranoid" = 3;
    "fs.protected_fifos" = 2;
    "fs.protected_regular" = 2;
    "vm.mmap_min_addr" = 65536;
    "dev.tty.ldisc_autoload" = 0;
  };

  # Enable AppArmor.
  security.apparmor.enable = true;
  security.apparmor.killUnconfinedConfinables = true;

  # If your machine connects to the local network with WiFi, uncomment this.
  # networking.wireless.enable = true;

  # Enable DHCP on all ethernet interfaces.
  networking.useNetworkd = true;
  systemd.network = {
    enable = true;
    networks.uplink = {
      matchConfig.Type = "ether";
      linkConfig.RequiredForOnline = "yes";
      networkConfig.DHCP = "yes";
    };
  };

  # No users can be added to the system after it's built.
  users.mutableUsers = false;

  # Only root can connect to the nix daemon.
  nix.settings.allowed-users = [ "root" ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # Do not forget to add an editor to edit configuration.nix ! The Nano
    # editor is also installed by default.
    vim
    wget
    speedtest-cli
  ];

  services.cron = {
    enable = true;
    systemCronJobs = [
      # Check every hour if we can ping the internet, otherwise reboot.
      "0 * * * * root ${testPing}/bin/test-ping"
      # In all cases, reboot once every month.
      "12 13 7 * * root /run/current-system/sw/bin/reboot"
    ];
  };

  # Enable automatic updates, to safely leave the system unattended.
  system.autoUpgrade.enable = true;
  # Allow the system to reboot if the kernel was updated, to make sure that
  # security patches are applied.
  system.autoUpgrade.allowReboot = true;
  # The channel is the version of the NixOS repository used for updates. For
  # each version, a "-small" channel exists which contains less precompiled
  # cached binaries than the full channel. Since recompilation of all packages
  # in the channel takes time, using the -small channel can sometimes reduce
  # the delay between a security patch publication and having it available in
  # NixOS.
  system.autoUpgrade.channel = "https://nixos.org/channels/nixos-23.11-small";

  # Automatically garbage collect old system generations.
  nix.gc.automatic = true;
  nix.gc.options = "--delete-older-than 30d";
}
