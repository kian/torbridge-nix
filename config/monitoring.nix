#
# This module sets up optional monitoring for the bridges.
# A prometheus agent is used to scrape exporters. The metrics are then sent to
# a remote prometheus server behind an authenticated onion service.
#

{ config, pkgs, lib, ... }:

with lib;
{

  options.torbridge.monitoring = {
    enable = mkOption {
      description = ''Enable collection of metrics on the bridge, and
                      sending of the metrics to a remote monitoring server.'';
      type = types.bool;
      default = true;
    };
    monitoringServerOnion = mkOption {
      description = ''Onion address of the remote monitoring server.'';
      type = types.nullOr types.str;
      default = null;
    };
  };


  config =
    let
      # Bind the config options to new names for use in the container config,
      # because the config variable name is shadowed in the container config
      # function.
      hostname = config.networking.hostName;
      monitoringServerOnion = config.torbridge.monitoring.monitoringServerOnion;
      monitoringServerOnionStrip = strings.removeSuffix ".onion" "${monitoringServerOnion}";
    in
    mkIf config.torbridge.monitoring.enable {

      containers."bridgecontrol".config = { config, pkgs, ... }: {

        # We run the prometheus agent in the container as it uses the tor
        # client daemon to send scrapped metrics to the monitoring server.
        services.prometheus = {
          enable = true;

          # Prometheus can be run in agent mode, a stripped down version of the
          # server that only scrapes metrics and sends them to a remote
          # prometheus server. Unfortunately, the current prometheus package in
          # nixpkgs doesn't support it. Idea for contribution FIXME ?
          #extraFlags = ["--enable-feature=agent"];
          extraFlags = [ "--enable-feature=no-default-scrape-port" ];

          listenAddress = "127.0.0.1";
          retentionTime = "1d";

          globalConfig = {
            scrape_interval = "1m";
            # Add labels to the scrapped metrics before sending them to the
            # remote server, to identify the bridge.
            external_labels = {
              "nickname" = hostname;
              "machine_type" = "torbridge";
            };
          };

          # Scrap a few exporters. We try to filter metrics and keep only
          # useful ones to limit bandwidth usage and storage space. More
          # exporters are configured depending on the bridge pluggable
          # transport, in the corresponding modules.
          scrapeConfigs = [
            {
              # Scrape remote storage metrics from the prometheus agent itself.
              # This allows to monitor the sending of metrics to the remote
              # server.
              job_name = "bridgeprometheus";
              static_configs = [{ targets = [ "127.0.0.1:9090" ]; }];
              metric_relabel_configs = [{
                source_labels = [ "__name__" ];
                regex = "prometheus_remote_storage_.*";
                action = "keep";
              }];
            }
            {
              # Scrape basic node metrics. Only keep a few useful metrics.
              job_name = "node_exporter";
              static_configs = [{ targets = [ "127.0.0.1:9100" ]; }];
              metric_relabel_configs = [{
                source_labels = [ "__name__" ];
                regex =
                  "system_version|node_(cpu_.*|memory_(MemTotal|MemFree|Cached|Buffers|Swap).*|filesystem_.*bytes|network_(receive|transmit)_bytes_total)";
                action = "keep";
              }];
            }
          ];

          remoteWrite = [{
            url =
              "http://${monitoringServerOnion}:17822/api/v1/write";
            proxy_url = "socks5://127.0.0.1:9050";
          }];
        };

        # The key pair used to authenticate to the monitoring onion service for
        # prometheus remote_writes is generated during the torbridge
        # installation procedure. Mix it with the remote onion address to
        # create an authentication file usable by tor.
        system.activationScripts.generate_onion_auth_file = ''
          mkdir -p /var/lib/tor/onion-auth
          echo "${monitoringServerOnionStrip}:descriptor:x25519:$(cat /root/secrets/metricskey.prv)" > /var/lib/tor/onion-auth/prometheus-remote.auth_private
          chown tor: -R /var/lib/tor/onion-auth
        '';
        # Storage location for the keys used to authenticate to onions
        # services.
        services.tor.settings.ClientOnionAuthDir = "/var/lib/tor/onion-auth";
      };

      # The authentication keys for the monitoring server are generated in the
      # main NixOS system. Copy them into the container so that they can be
      # used by the tor daemon handling remote_write traffic to the monitoring
      # server.
      system.activationScripts.inject_onion_auth = ''
        mkdir -p /var/lib/nixos-containers/bridgecontrol/root/secrets
        cp -r /root/secrets/metricskey.prv   \
              /var/lib/nixos-containers/bridgecontrol/root/secrets
      '';

      # Write system version to a file read by prometheus node exporter, to
      # monitor NixOS rebuilds.
      system.activationScripts.node-exporter-system-version = ''
        mkdir -pm 0775 /var/lib/prometheus-node-exporter-text-files
        (
          cd /var/lib/prometheus-node-exporter-text-files
          (
            echo -n "system_version ";
            readlink /nix/var/nix/profiles/system | cut -d- -f2
          ) > system-version.prom.next
          mv system-version.prom.next system-version.prom
        )
      '';

      # Run prometheus node exporter.
      services.prometheus.exporters.node = {
        enable = true;
        listenAddress = "127.0.0.1";
        extraFlags = [
          "--collector.disable-defaults"
          "--collector.textfile.directory=/var/lib/prometheus-node-exporter-text-files"
        ];
        enabledCollectors = [ "textfile" "cpu" "meminfo" "netdev" "filesystem" ];
      };
    };
}
