#
# This module configures the tor bridge with obfs4 pluggable transport. It also
# configures an exporter to expose metrics.
#

{ config, pkgs, lib, ... }:

let
  enable = config.torbridge.pluggableTransport == "obfs4";
in
with lib;
{
  options.torbridge.obfs4 = {
    orport = mkOption {
      description = ''Port used to receive relay traffic when running 
                      as an obfs4 bridge.'';
      type = types.port;
    };

    obfs4port = mkOption {
      description = "Port on which the obfs4 bridge listens for connections.";
      type = types.port;
    };
  };

  config = mkIf enable (mkMerge [

    {
      # Open ports used by the bridge in the firewall.
      networking.firewall = {
        allowedTCPPorts = [
          config.torbridge.obfs4.orport
          config.torbridge.obfs4.obfs4port
        ];
      };

      # The tor bridge needs to be reachable on two ports. One to receive
      # obfuscated connections from users (obfs4port), and one for relaying
      # traffic with the rest of the Tor network (orport).
      services.tor = {
        enable = true;

        # Activate the control socket for the tor-prometheus_exporter.
        controlSocket.enable = true;

        # Configure the tor daemon as a bridge.
        relay = {
          enable = true;
          role = "bridge";
        };
        settings = {
          BridgeRelay = true;
          ServerTransportPlugin = {
            transports = [ "obfs4" ];
            exec = "${pkgs.obfs4}/bin/obfs4proxy";
          };
          ORPort = config.torbridge.obfs4.orport;
          ServerTransportListenAddr =
            "obfs4 0.0.0.0:${toString config.torbridge.obfs4.obfs4port}";
          # You can limit the bandwidth rate used by the bridge. Home routers
          # typically have asymmetric bandwith, with larger rate in download
          # than upload. If that's the case, the two parameters should be set
          # to a value smaller than the upload bandwidth. Default value is
          # 1GByte.
          BandwidthRate = "500 MBytes";
          BandwidthBurst = "500 MBytes";
          # If the home router does not support IPv6, you can disable it to
          # avoid logs flood by unsuccessufull IPv6 configuration.
          AddressDisableIPv6 = true;
          # Raise log level for the CONTROL segment to avoid flooding the logs
          # with "control connection open" on every connection of the exporter.
          Log = "[~control]notice warn";
          LogMessageDomains = true;
        };
      };

      # Open the ports used by the bridge on the home router using UPnP. See
      # upnpc.nix for details.
      torbridge.openPortsUPnP = [
        config.torbridge.obfs4.orport
        config.torbridge.obfs4.obfs4port
      ];
    }

    # Configure monitoring.
    (mkIf config.torbridge.monitoring.enable {

      # Enable prometheus exporter for the tor daemon acting as a bridge.
      services.prometheus.exporters.tor = {
        enable = true;
        extraFlags = [ "--mode=unix" "--control-socket=/run/tor/control" ];
        listenAddress = "127.0.0.1";
        port = 9130;
      };

      # Add the daemon to tor group to grant it access to the tor control
      # socket.
      systemd.services."prometheus-tor-exporter".serviceConfig = {
        SupplementaryGroups = "tor";
        RestrictAddressFamilies = [ "AF_UNIX" ];
      };

      # Add tor exporter as a scrape target for prometheus. Filter out useless
      # metrics.
      containers.bridgecontrol.config.services.prometheus.scrapeConfigs = [
        {
          job_name = "torbridge";
          static_configs = [{ targets = [ "127.0.0.1:9130" ]; }];
          metric_relabel_configs = [{
            source_labels = [ "__name__" ];
            regex = "tor_.*";
            action = "keep";
          }];
        }
      ];
    })

  ]);
}
