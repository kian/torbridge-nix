#
# Use this file to store local configuration changes. This makes it easier to
# update the base configuration without overriding you local settings.
#
# Configuration options already defined in other files can be overrided with
# the mkForce function.
# Eg: `networking.hostName = lib.mkForce "my-better-hostname";`.
#

{ config, pkgs, lib, ... }:

{
  # Use a SSH key to log-in as root on the monitoring server.
  # This key should be well protected.
  # Copy here the content of the public key file.
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3Nza...rARF6j+"
  ];

  time.timeZone = "Europe/Berlin";
  console.keyMap = "de";

  # The device on which NixOS is installed.
  # This value is set by the monitoring server installation script.
  boot.loader.grub.device = "/dev/sda";
}
