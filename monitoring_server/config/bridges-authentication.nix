#
# This file contains the list of public keys that are allowed to connect to the
# onion service exposing the central prometheus server.
#
# After a new bridge is created with the bridge installation script, add its
# public key here and rebuild the monitoring server configuration to allow
# metrics collection.
#

{ config, pkgs, ... }:

{
  # For each bridge, add a string to the list of the form:
  # "descriptor:x25519:<bridge public key>"
  services.tor.relay.onionServices."prometheus".authorizedClients = [
    # Add descriptors here, separated by newlines.
  ];
}
