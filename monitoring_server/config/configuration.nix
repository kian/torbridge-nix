# 
# This file contains the generic configuration for a torbridge monitoring
# server. Local modifications should be maid in local.nix.
# 
# The monitoring server exposes onion services for:
#   - a ssh server to allow remote root access,
#   - a prometheus server that collects metrics sent by the bridge,
#   - a grafana server to display metrics in dashboards.
#
# The onion service that exposes the prometheus server is authenticated.
# Bridges must be registered to the monitoring server by adding their public
# key in `bridges-authentication.nix`. The bridge public key is generated
# during the installation process.
#

{ config, pkgs, ... }:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    # Include local configuration.
    ./local.nix
    # Include bridge public keys to allow them to send metrics to the
    # prometheus server.
    ./bridges-authentication.nix
  ];

  # We use a hardened kernel from the Nixos repositories. It is not very clear
  # whether this is useful with the explicit (and more restrictive) boot and
  # sysctl parameters we set after that, but it certainly cannot hurt.
  # In order to improve stability and limit the need of reboots after upgrades,
  # we freeze the kernel version to 6.1 which is a LTS that will receive
  # security updates until December 2026.
  # If you want to run a bridge on very recent hardware, you might need to use
  # a more recent kernel with updated drivers.
  boot.kernelPackages = pkgs.linuxPackages_6_1_hardened;

  boot.kernelParams = [
    "page_poison=1"
    "slab_nomerge"
    "slub_debug=FZP"
    "mce=0"
    "pti=on"
    "mds=full,nosmt"
    "init_on_free=1"
    "init_on_alloc=1"
    "lockdown=confidentiality"
    "intel_iommu=on"
    "amd_iommu=on"
    "modules.sig_enforce=1"
    "vsyscall=none"
    "oops=panic"
  ];

  boot.kernel.sysctl = {
    # hardening
    "kernel.dmesg_restrict" = 1;
    "kernel.kptr_restrict" = 2;
    "kernel.unprivileged_bpf_disabled" = 1;
    "net.core.bpf_jit_harden" = 2;
    "kernel.yama.ptrace_scope" = 2;
    "kernel.kexec_load_disabled" = 1;
    "net.ipv4.tcp_syncookies" = 1;
    "net.ipv4.tcp_rfc1337" = 1;
    "net.ipv4.conf.default.rp_filter" = 1;
    "net.ipv4.conf.all.rp_filter" = 1;
    "net.ipv4.conf.all.accept_redirects" = 0;
    "net.ipv4.conf.default.accept_redirects" = 0;
    "net.ipv4.conf.all.secure_redirects" = 0;
    "net.ipv4.conf.default.secure_redirects" = 0;
    "net.ipv6.conf.all.accept_redirects" = 0;
    "net.ipv6.conf.default.accept_redirects" = 0;
    "net.ipv4.conf.all.send_redirects" = 0;
    "net.ipv4.conf.default.send_redirects" = 0;
    "net.ipv4.icmp_echo_ignore_all" = 1;
    "vm.mmap_rnd_bits" = 32;
    "vm.mmap_rnd_compat_bits" = 16;
    #"net.ipv4.tcp_timestamps" = 0;
    #"net.ipv4.tcp_sack" = 0;
    "kernel.sysrq" = 0;
    "kernel.deny_new_usb" = 1;
    "kernel.panic" = 10;
    "net.ipv4.conf.all.arp_filter" = 1;
    "net.ipv4.conf.all.arp_ignore" = 2;
    "net.ipv4.conf.all.arp_announce" = 2;
    "net.ipv4.conf.all.drop_gratuitous_arp" = 1;
    "net.ipv4.tcp_syn_retries" = 3;
    "net.ipv4.tcp_synack_retries" = 3;
    "kernel.panic_on_oops" = 1;
    "kernel.perf_event_max_sample_rate" = 1;
    "kernel.perf_cpu_time_max_percent" = 1;
    "kernel.perf_event_paranoid" = 3;
    "fs.protected_fifos" = 2;
    "fs.protected_regular" = 2;
    "vm.mmap_min_addr" = 65536;
    "dev.tty.ldisc_autoload" = 0;
  };

  # Bootloader configuration. We use legacy BIOS to ensure maximum hardware
  # compabtibility.
  boot.loader.grub.enable = true;

  # Machine host name.
  networking.hostName = "bridgemonitoring";

  # Prevent users modification after system build.
  users.mutableUsers = false;

  # Start a SSH server.
  services.openssh.enable = true;

  # Install some packages.
  environment.systemPackages = with pkgs; [ vim wget htop git ];
  # Choose default editor.
  programs.vim.defaultEditor = true;

  # Enable automatic upgrades of NixOS.
  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    channel = "https://nixos.org/channels/nixos-23.05";
  };

  # Enable automatic garbage collection of old NixOS generations.
  nix.gc = {
    automatic = true;
    options = "--delete-older-than 30d";
  };

  # ---- bridge montioring ----

  # Tor daemon to expose onion services.
  services.tor = {
    enable = true;
    # FIXME: maybe useless
    client.enable = true;

    settings = {
      #Log = "info stderr";
      # Do not wait on shutdown : the daemon is not a relay, so it doesn't need
      # extra time to gracefully close client circuits.
      ShutdownWaitLength = 1;
    };

    # Configure an onion service for the SSH and Grafana servers.
    relay.onionServices."bridgemetrics" = {
      # Use a provisionned private key to generate a deterministic onion
      # address for the service. The private key need to be already present on
      # the machine, only readable by root.
      secretKey = "/root/secrets/onionkeys/bridgemetrics/hs_ed25519_secret_key";
      map = [
        # Expose Grafana.
        {
          port = 80;
          target = {
            #unix = "/run/grafana/grafana.sock";
            addr = "127.0.0.1";
            port = 3000;
          };
        }
        # Expose SSH.
        {
          port = 22;
          target = {
            addr = "127.0.0.1";
            port = 22;
          };
        }
      ];
    };

    # Configure an onion service for Prometheus to receive remote writes.
    # This onion service is authenticated. Acces is authorized by adding
    # bridges public keys to `bridges-authentication.nix`.
    relay.onionServices."prometheus" = {
      # Use a provisionned private key to generate a deterministic onion
      # address for the service. The private key need to be already present on
      # the machine, only readable by root.
      secretKey = "/root/secrets/onionkeys/prometheus/hs_ed25519_secret_key";
      map = [{
        port = 17822;
        target = {
          addr = "127.0.0.1";
          port = 9090;
        };
      }];
    };
  };

  # FIXME: is this necessary ?
  systemd.services.tor.serviceConfig = {
    ReadWritePaths = [ "/run/grafana" ];
    BindPaths = [ "/run/grafana" ];
    SupplementaryGroups = [ "grafana" ];
  };

  # Configure a prometheus server to receive remote writes from the bridges.
  services.prometheus = {
    enable = true;
    # Maximum retention time for metrics.
    retentionTime = "10y";
    extraFlags = [
      "--web.enable-remote-write-receiver"
      # Maximum size of the metrics database.
      "--storage.tsdb.retention.size=50GB"
    ];

    # NixOS options do not expose the storage.tsdb.out_of_order_time_window
    # option, so we have to resort to writing the prometheus configuration
    # ourselves. Idea for a PR ? The `out_of_order_time_window` option tells
    # the promehteus server to accept metrics sample from remote writes for a
    # certain time window even if more recent samples have already been
    # received. It makes remote write over relatively unreliable tor
    # connections more stable.
    configText = ''
      global:
        scrape_interval: 60s

      scrape_configs:
        - job_name: 'node_exporter_local'
          static_configs:
            - targets: ['127.0.0.1:9100']
              labels:
                nickname: bridgemonitoring
                machine_type: telemetry

      storage:
        tsdb:
          out_of_order_time_window: 1d
    '';
  };

  # Configure a Grafana server to display metrics in nice dashboards.
  services.grafana = {
    enable = true;
    #settings.server.protocol = "socket";  # causes SIGSYS (why?)
    settings.server.http_addr = "127.0.0.1";

    provision.enable = true;
    provision.datasources.settings.datasources = [{
      name = "DS_PROMETHEUS";
      type = "prometheus";
      url = "http://127.0.0.1:9090";
      # To make the dashboards more easily portable, we fix the UID of the
      # datasource. This way, the json describing exported dashboard can be
      # reused in other grafana instances provided that there is a datasource
      # with the same UID.
      uid = "33e28f7f-e577-43b2-96f2-75a4dd3cc61d";
    }];

    # Instruct Grafana to look for provisioned dashboards in a specific
    # directory.
    provision.dashboards.settings.providers = [{
      name = "localfiles";
      options.path = "/var/lib/grafana/dashboards";
      # The option `allowUiUpdates` can be used to prevent dashboards edits
      # from the Web UI. When someone tries to save a modified dashboard, a
      # message is displayed that informs them to update to provisionned json
      # definition instead. It would be nice to have this, since changes are
      # then really persistent and not lost upon configuration reload.
      # The NixOS Grafana module does not expose this option. It actually
      # exposes very few, and the documentation of the options suggests a
      # confusion between dashboard providers and dashboard provisions. See
      # https://grafana.com/docs/grafana/latest/administration/provisioning/#dashboards
      # FIXME: Idea for a PR ?
      # We could use provision.dashboards.path instead and write the full yaml
      # ourselves, but that's not worth the trouble right now !
      #allowUiUpdates = false;
    }];
  };

  # Copy the dashboards files to the provision directory.
  system.activationScripts."provision-dashboards" =
    ''
      mkdir -p /var/lib/grafana/dashboards
      cp /root/dashboards/* /var/lib/grafana/dashboards/
    '';

  # Collect metrics about the monitoring server itself.
  services.prometheus.exporters.node = {
    enable = true;
    listenAddress = "127.0.0.1";
    extraFlags = [
      "--collector.disable-defaults"
      "--collector.textfile.directory=/var/lib/prometheus-node-exporter-text-files"
    ];
    enabledCollectors = [ "textfile" "cpu" "meminfo" "netdev" "filesystem" ];
  };

  # Write system version to a file read by prometheus node exporter, to
  # monitor NixOS rebuilds.
  system.activationScripts.node-exporter-system-version = ''
    mkdir -pm 0775 /var/lib/prometheus-node-exporter-text-files
    (
     cd /var/lib/prometheus-node-exporter-text-files
     (
      echo -n "system_version ";
      readlink /nix/var/nix/profiles/system | cut -d- -f2
     ) > system-version.prom.next
     mv system-version.prom.next system-version.prom
     )
  '';

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}

