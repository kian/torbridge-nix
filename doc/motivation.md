# Pre-installed Tor bridges

## Circumventing censorship

In many places around the world, access to the internet is heavily surveilled
and censored. Under authoritarian regimes and/or during political uprisings,
authorities routinely censor social media, opinionated blogs, international
news websites and anything that supports organisation of their opponents [ref].
In addition, the online activities of individuals are monitored and those
identified as "opponents" face -- sometimes life-threatening -- repression
[ref]. In such cases, anonymous connection to the internet is vital.

The tor network can be used to achieve this.

## Tor in a nutshell

The tor network is made of thousands of relays operated by volunteers around
the world. When one connects to the network, the tor program running on their
machine chooses at random a circuit of three relays in the network. Then, all
connections are proxied by the relays in the circuit before reaching their
final destination. The first relay of the circuit knows the address that
initiated the connection, but does not know where it goes to (it only knows the
next relay in the circuit), and the last relay of the circuit knows where the
connections is heading to, but has no idea of who initiated it (it only knows
that it received it from the previous relay in the circuit). This achieve
anonymous connection to the internet !

For many use cases, this is good enough. For instance, if you live in a rather
democratic country and want to access the internet anonymously to avoid mass
surveillance and aggressive marketing profiling, or escape from a censored
corporate/institutional network, accessing the tor network this way will do the
job (the Tor Browser, which connects automatically to the tor network, is super
easy to install and use : [link]).

If you live under heavy censorship however, there is a catch. In order to let
the tor program running on our machines choose the relays for its circuits, the
address of these relays have to be publicly known. Therefore, it is easy for an
authority to censor the tor network itself by simply blocking all connections
to the public relays.

To address this issue, hidden tor relays exist in addition to public ones. The
hidden relays are called *bridges* and serve as hidden entry points to the tor
network (they are the first relay in the circuit). People who need them can
request bridges through various automatic channels, that implement
counter-measures to prevent an malicious user to enumerate a large portion of
the available bridges. If you need a bridge, check the official documentation
[ref].

## Let's host bridges !

Both public relays and bridges are hosted by volunteers. Since the beginning
of the war in Ukraine, and later with the uprising in Iran, the number of
connections to bridges has grown a lot but the number of available bridges
increased much slower. Hence, there is a huge need for more bridges (and more
tor relays in general).

This motivated us to pre-configure machines that would automatically set-up a
tor bridge when plugged in a home network, without requiring any technical
skill. This repo contains the system we came up with. Feel free to use it: the
more bridges the better !
