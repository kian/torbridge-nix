# Torbridges

A tool to manage a pool of tor bridges behind home routers.

## Overview

We use NixOS to quickly pre-install machines that run a tor bridge when
connected to a home network. The idea is that the machines are configured once
by someone who feels confident enough with computers to do it, and then handed
over to anyone who is happy to share their internet bandwidth to fight
censorship. People hosting the bridges simply have to connect the machine with
an ethernet cable to their home router.

We also provide configuration for a simple monitoring server. It can be used by
the deployment team to check that everything is running smoothly.

Hopefully, this can help people with computing know-how to spread tor bridges
in their communities with minimal hassle.

## Contributing

Contributions, discussions and feedback are most welcome. Don't hesitate to
open pull requests and issues.

## Dependencies

All connections to the bridges or the monitoring server are made over the tor
network, to avoid making a link between the machines. Hence, you need tor
installed on your machine.

If you want to build the bootstrap image yourself, you will also need nix.

## Deploying a bridge pool

We try to make the deployment as straightforward as possible by reducing to a
minimum the number of options. Yet, we do our best to make the full
configuration easy to hack (see [Hacking](#hacking)).

### Updates workflow

This is a very small project, with no release versions. We try to ensure that
the configuration in the `master` branch is always functionnal. To ease
updating from this repo, we propose that you clone it and create a new branch
for your deployment. To update from upstream, pull the new version of the
`master` branch and `git rebase` your branch onto it.

### Building or getting the bootstrap image

The bootstrap image is live minimal NixOS system with a SSH server allowing
root access with a deployment key, used by the installation scripts to setup
the full systems (bridge or monitoring server).

If you have `nix` installed on your machine, you can build the bootstrap image
yourself by running `bootstrap/generate-iso`. The path to the generated image
in the nix store is the last output line.

If you don't have `nix` installed, or don't want to build the bootstrap image
yourself, you can find it at FIXME.

Once you have the boostrap image, create a bootable flash drive with `dd
if=bootstrap/bootstrap.iso of=<flash device> bs=1M status=progress`.

### Creating a management SSH key pair

We recommend using a dedicated SSH key pair for the bridge pool management.
This key grants root access to all bridges and monitoring server. Hence, it
should be protected with a strong passphrase.

### Setting-up the monitoring server

The monitoring server is optional, but highly recommended. You probably don't
want the bridges you have given to people around you to stop working for months
without you knowing.

- Put the path to your management SSH key in the `SSHKEY` variable of
  `monitoring_server/util/sshtool` and
`monitoring_server/util/ssh-monitoring-server`.

- Make sure that you have the bootstrap image (see [Building or getting the
  bootstrap image](#building-or-getting-the-bootstrap-image)).

- Boot the target machine on the bootstrap image and get its IP (you can
  log-in as root without password).

- Run `monitoring_server/install/install-monitoring-server <IP address>` and
  follow the procedure.

- Connect to the Grafana onion service. The address can be found in
  `monitoring-server/data/bridgemetrics-onion-address`. Log-in as `admin` with
  default `admin` password. Then choose a new strong password.

### Deploying a new monitoring server configuration

FIXME: Only run the util scripts when inside the util directory.

To deploy a modified configuration on the monitoring server, run
`monitoring_server/util/deploy-config`.

To check that the modified NixOS configuration builds without error, without
deploying it, run `monitoring_server/util/build-config`. Note that if the
configuration build fails, the running system is not modified, so it is not a
problem to deploy without checking the build. => FIXME: Remove this script ?

Changing NixOS release version amounts to changing the channel from which
modules are pulled. To change the NixOS release, change the channel in
`monitoring_server/util/change-nixos-channel` and run the script. => FIXME:
Put the nix-channel commands in `deploy-config` and let upstream take care of
bumping the NixOS version when appropriate ?

### Setting bridges common configuration

- Put the path to your management SSH key in the `SSHKEY` variable of
  `util/sshtool` and `util/ssh-monitoring-server`.

- Edit `bridge-configs/common.nix`. It contains the configuration variables
  that are common to all bridges.

### Installing a new bridge

- If you want to collect bridge metrics on a monitoring server, make sure that
  the monitoring server is in place. See [Setting-up a monitoring
  server](#setting-up-a-monitoring-server).

- Make sure that the configuration common to all bridges is in place (see
  [Setting bridges common
  configuration](#setting-bridges-common-configuration)).

- Make sure that you have the bootstrap image (see [Building or getting the
  bootstrap image](#building-or-getting-the-bootstrap-image)).

- Boot the target machine on the bootstrap image and get its IP (you can
  log-in as root without password).

- Run `install/install-torbridge <bridge name> <IP address>` and follow the
  procedure.

- If the bridge was configured with `torbridge.monitoring.enable = true`,
  authorise it on the monitoring server. See [Authorising a bridge on the
  monitoring server](#authorising-a-bridge-on-the-monitoring-server).

### Authorising a bridge on the monitoring server

- Get the bridge monitoring public key in `bridges_config/data/<bridge
  name>/monitoring-pubkey`.

- Add a line in `monitoring_server/config/bridge-authentication.nix` of the
  form `"descriptor:x25519:<bridge public key>`.

- Rebuild the monitoring server's configuration. See [Deployin a new monitoring
  server configuration](#deploying-a-new-monitoring-server-configuration).

### Update bridge configurations

FIXME: Only run the util scripts when inside the util directory.

To deploy a modified configuration on a bridge, run `util/deploy-config <bridge
names>`. If no bridge name is provided, the configuration is deployed on all
bridges that can be found in `brigdge_configs/`.

To check that the modified NixOS configuration builds without error, without
deploying it, run `util/build-config`. Note that if the configuration build
fails, the running system is not modified, so it is not a problem to deploy
without checking the build. => FIXME: Remove this script ?

Changing NixOS release version amounts to changing the channel from which
modules are pulled. To change the NixOS release, change the channel in
`util/change-nixos-channel` and run the script. => FIXME: Put the nix-channel
commands in `deploy-config` and let upstream take care of bumping the NixOS
version when appropriate ?

## Hacking

The overall system looks somewhat like this:

![system schematics](doc/drawing.svg)

The NixOS configuration is purely declarative and fully contained within the
configuration files. We try to sort the configuration values in meaningfull
sections and separated files. We try to adopt a litterate programming approach
and make heavy use of comments with the hope that the configuration files are
self-documented.

search.nixos.org is convenient to quickly look for available configuration
options. NixOS doesn't really care about where configuration variables are set.
In the end, everything is collected to build a large dictionnary that describes
the full system. Hence, we recommend that you leave the base configuration
files untouched and make changes either in the bridge-specific files in
`bridges_config/` or in the configuration common to all bridges in
`bridges_config/common.nix`. This makes it easier to upgrade the base
configuration without loosing your local settings.

To override a configuration variable that is already set elsewhere, you can use
the `mkForce` function. For instance:

```
services.tor.enable = lib.mkForce false;
```

## Threat model

Tor bridges have access to the IP address of the people using them to defeat
censorship. We don't want this information to be leaked, as this could endanger
bridge users.
> Remote access to the bridge requires the management SSH key. Hence bridges
> users security is no better than the SSH key security, which should be well
> protected. IP of the clients are (obviously) not logged. The metrics sent
> over to the monitoring server include counter of connections by geoIP
> countries. We collect those metrics directly from the tor daemon which only
> increment the counters by multiple of 8 and resets them every 24h. We assume
> that these aggregated metrics provided by the tor daemon are acceptable to
> collect, and consider that they provide valuable insight into the usefulness
> of tor bridges. Nontheless, the monitoring server should be well protected as
> well, by protecting the management SSH key and choosing a strong password
> for the web Grafana access.

Tor bridges serve as hidden entrypoints to the Tor network in places where
traffic to the publicly known Tor relays is blocked. To make it harder for
censors to identify and block bridges, we want to avoid signatures that
could link several IPs to a bridge pool.
> The SSH server on the bridges is exposed behind an onion service. This
> ensures anonymity of remote accesses. The bridges send metrics to the
> monitoring server anonymously over an (authenticated) onion service.

Censors can request bridges in the same way as legitimate users do. Hence, the
public IP of the home router hosting a bridge might at some point become known
to an adversary. If the adversary motivation is censorship, they are most
likely to just block the IP and go one with their busy censor life. However,
they might also try attacks on this IP. We don't want the presence of a bridge
to weaken the security of the hosting home network.
> The bridges run a hardened kernel.
> The bridge only opens two ports on the home router (OR and OBFS4) behind
> which the tor daemon listens. In addition, an OpenSSH server is exposed
> behind an onion service. Hence, stepping in the home network from the bridge
> would require compromission of the tor daemon or compromission of the OpenSSH
> daemon or compromission of the management SSH key. We believe that the risk
> of the first two is rather low, and cannot be avoided. The latter risk seems
> more likely and can be mitigated by protecting the management key with a
> strong passphrase.

## Stability, resilience

We try hard to make the bridges as stable and resilient as possible. Since they
run in uncontrolled environment (eg: your grandma's home), they might face all
sorts of adversarial events:
- ISP upstream connection downtime
- Power cuts
- Temporary unplug by the hoster
- Hardware failure
- ...

Most of the time, a reboot brings the bridge back to operational state. 

We recommend keeping an eye on the global bridge monitoring dashboard of the
monitoring server to identify failing bridges quickly. Failing bridges are
usually also inaccessible remotely. In such case, we find that the easiest
solution if to contact the hoster, ask if they know what happened (sometimes,
they unplug the machine themselves for all sorts of good reasons) and asked
them to reboot the machine manually ("unplug/plug" works).

Some measures are implemented to improve the bridge stability:
- Regularly ping Quad9. If ping fails for some time, reboot.
- Reboot once a month, no matter what.

Remote access uses an SSH server exposed behind an onion service. Connections
over the tor network are not always very reliable. It might happen that you
cannot connect to some bridges from time to time. 

When this happens, we recommend waiting a bit and trying again later. If the
issue persists, contact the hoster and ask for a reboot.

**Improving the reliability of the bridges is paramount : it reduces work for the
management team, limit the number of times the hosters are involved, and most
importantly, ensures that bridges are running smoothly for users who need them.
If you have ideas or feedbacks on recurrent failures in your bridge pool,
please reach out !**

## Troubleshooting

- As long as the public key of the bridge is not authorized in the monitoring
server's onion service, the bridge tor daemon in the bridgecontrol container
spits warnings:
```
Client authorization for requested onion address is invalid. Can't decrypt the
descriptor.
Closed 1 streams for service [scrubbed].onion for reason resolve failed. Fetch
status: No more HSDir available to query.
```
This is fixec by authorising the bridge in the monitoring server configuration.
See [Authorising a bridge on the monitoring
server](#authorising-a-bridge-on-the-monitoring-server).

- Some Internet Service Providers only offer "a quarter of a public IP" to
  their customers. Only a quarter of the ports range can be used
by the home router and the bridges will not be reachable if the randomly chosen
OR and OBFS4 ports are outside of the range. In such case, it is often possible
to request a "full public IP" from the ISP website for free. You can also
change `torbridge.obfs4.orport` and `torbridge.obfs4.obfs4port` in the bridge
configuration file.

## TODO

- Deal with FIXMEs.
- Make the util scripts work independently from where they are called.
- Simplify the installation procedures by removing phases 0 and 1. Everything
  can be done from the install script. Provision keys directly on the live
system and install the final configuration directly. Saves one reboot, and is
likely faster.
- Expose `allowUiUpdates` provisioning options in the NixOS Grafana module.
- Limit the duration of UPnP forwards to avoid downtime if bridges change IP in
  the home network.
- Ask for the management ssh key in only one place.
- Add options for timezone and keymap in common.nix.
- Add a module for a standalone snowflake bridges ?
