{ config, lib, pkgs, ... }: 

{

  imports = [];

  networking.hostName = "bootstrap-sshd";

  environment.systemPackages = with pkgs; [
    vim
  ];

  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";
  };

  users.users.root = {
    password = "";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILfbhEsYX/5zg083k8vztajjAYN+jSll+4swR56ChbkD"
    ];
  };

  system.stateVersion = lib.version;
}
